import React from 'react';
import './AddTaskForm.css';

const TaskForm = props => {
    return (
        <form action="#" className="task-form">
            <input value={props.text} type="text" placeholder="Add new task" onChange={props.newTaskText}/>
            <button onClick={props.newTask}>Add</button>
        </form>
    )
};

export default TaskForm;