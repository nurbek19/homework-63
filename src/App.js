import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import HomePage from './containers/HomePage/HomePage';
import TodoList from './containers/TodoList/TodoList';
import MovieList from './containers/MovieList/MovieList';
import './App.css';

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact component={HomePage}/>
        <Route path="/todo-list" component={TodoList}/>
        <Route path="/movie-list" component={MovieList}/>
      </Switch>
    );
  }
}

export default App;
