import React, {Component} from 'react';
import Movie from '../../components/Movie/Movie';
import './MovieList.css';

import axios from 'axios';

class MovieList extends Component {
    state = {
        movies: [],
        currentMovie: {
            movie: ''
        },
        loading: false
    };

    addTitle = (event) => {
        let currentMovie = this.state.currentMovie;
        currentMovie.movie = event.target.value;

        this.setState({currentMovie});
    };

    addMovie = (e) => {
        e.preventDefault();
        let currentMovie = this.state.currentMovie;

        if (currentMovie.movie !== '') {

            this.setState({loading: true});

            axios.post('/movies.json', this.state.currentMovie).then(() => {
                currentMovie.movie = '';

                this.setState({loading: false, currentMovie});
                this.getMovies();
            });

        } else {
            alert('Please add movie!');
        }
    };

    editMovie = (event, id) => {
        const index = this.state.movies.findIndex(m => m.id === id);
        const movie = {...this.state.movies[index]};
        const movies = [...this.state.movies];

        movie.movie = event.target.value;
        movies[index] = movie;

        this.setState({movies});
    };

    changeMovie = id => {
        const index = this.state.movies.findIndex(m => m.id === id);

        axios.put(`movies/${id}.json`, {...this.state.movies[index]}).then(response => {
            const movies = [...this.state.movies];
            movies[index] = response.data;

            this.setState({movies});
        });
    };

    removeMovie = (id) => {
        axios.delete(`movies/${id}.json`).then(() => {
            this.setState(prevState => {
                const movies = [...prevState.movies];
                const index = movies.findIndex(movie => movie.id === id);
                movies.splice(index, 1);
                return {movies};
            })
        });
    };

    getMovies = () => {
        axios.get('/movies.json').then(response => {
            const movies = [];

            for (let key in response.data) {
                movies.unshift({...response.data[key], id: key});
            }
            this.setState({movies});
        });
    };

    componentDidMount() {
        this.getMovies();
    }

    render() {

        let moviesList = this.state.movies.map((movie) => {
            return <Movie
                title={movie.movie}
                key={movie.id}
                edit={(event) => this.editMovie(event, movie.id)}
                change={() => this.changeMovie(movie.id)}
                remove={() => this.removeMovie(movie.id)}
            />
        });

        if (this.state.loading) {
            moviesList = <p>Loading...</p>;
        }

        return (
            <div className="MovieList">
                <form action="#">
                    <input
                        type="text"
                        placeholder="Add movie"
                        onChange={this.addTitle}
                        value={this.state.currentMovie.movie}
                    />
                    <button onClick={this.addMovie}>Add</button>
                </form>

                <div className="Movies">
                    <h4>To watch list:</h4>
                    {moviesList}
                </div>
            </div>
        )
    }
}

export default MovieList;