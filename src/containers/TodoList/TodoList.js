import React, {Component} from 'react';
import AddTaskForm from '../../components/AddTaskForm/AddTaskForm';
import Task from '../../components/Task/Task';
import axios from 'axios';
import './TodoList.css';

class TodoList extends Component {
    state = {
        tasks: [],
        newTask: {
            text: ''
        },
        loading: false
    };

    addNewTaskText = (event) => {
        const newTask = this.state.newTask;
        newTask.text = event.target.value;

        this.setState({newTask});
    };

    addNewTask = e => {
        e.preventDefault();

        if (this.state.newTask.text !== '') {
            this.setState({loading: true});

            axios.post('/tasks.json', this.state.newTask).then(() => {
                const newTask = this.state.newTask;
                newTask.text = '';

                this.setState({loading: false, newTask});

                this.getTasks();
            });
        } else {
            alert('Please add task text!');
        }

    };

    getTasks = () => {
        axios.get('/tasks.json').then(response => {
            const tasks = [];

            for (let key in response.data) {
                tasks.unshift({...response.data[key], id: key});
            }
            this.setState({tasks});
        });
    };

    removeTask = (id) => {
        axios.delete(`tasks/${id}.json`).then(() => {
            this.setState(prevState => {
                const tasks = [...prevState.tasks];
                const index = tasks.findIndex(task => task.id === id);
                tasks.splice(index, 1);
                return {tasks};
            })
        });
    };

    componentDidMount() {
        this.getTasks();
    }


    render() {
        let tasks = (
            <div className="tasks">
                {this.state.tasks.map((task) => {
                    return <Task
                        key={task.id}
                        task={task.text}
                        remove={() => this.removeTask(task.id)}
                    />
                })}
            </div>
        );

        if (this.state.loading) {
            tasks = <p>Loading...</p>;
        }

        return (
            <div className="App">
                <div className="todo-list">
                    <AddTaskForm
                        newTaskText={this.addNewTaskText}
                        newTask={this.addNewTask}
                        text={this.state.newTask.text}
                    />

                    {tasks}
                </div>
            </div>
        );
    }
}

export default TodoList;