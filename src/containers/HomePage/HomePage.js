import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import './HomePage.css';

class HomePage extends Component {
    render() {
        return(
            <div className="home-page">
                <NavLink to="/todo-list">Todo List</NavLink>
                <NavLink to="/movie-list">Movie List</NavLink>
            </div>
        )
    }
}

export default HomePage;